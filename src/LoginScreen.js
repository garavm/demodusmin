import React, {useState} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Button,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {TextInputMask} from 'react-native-masked-text';

const {width, height} = Dimensions.get('window');
const LoginScreen = (props) => {
  const [mobileNumber, setMobileNumber] = useState('');
  const onSubmit = () => {
    props.navigation.navigate('Product');
  };
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={{paddingVertical: 12, fontSize: 18}}>Phone number</Text>
        <TextInputMask
          style={styles.input}
          keyboardType="numeric"
          placeholder="xxx-xxx-xxxx"
          type={'custom'}
          options={{
            mask: '999-999-9999',
          }}
          value={mobileNumber}
          onChangeText={(text) => setMobileNumber(text)}
        />
      </View>

      <TouchableOpacity
        style={{backgroundColor: 'green', marginVertical: 15, borderRadius: 24}}
        onPress={onSubmit}>
        <Text
          style={{
            paddingVertical: 8,
            paddingHorizontal: 16,
            color: 'white',
            fontSize: 24,
          }}>
          Login
        </Text>
      </TouchableOpacity>
    </View>
  );
};

LoginScreen.propTypes = {};
const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  input: {
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 4,
    width: width - 32,
    paddingVertical: 12,
    paddingHorizontal: 12,
    fontSize: 24,
  },
  content: {
    alignItems: 'flex-start',
  },
});

export default LoginScreen;
