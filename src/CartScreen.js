import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';

const Item = ({item}) => (
  <View style={styles.itemCointainer}>
    <Image source={item.uri} style={styles.image} />
    <View style={styles.verticalLine} />
    <View style={styles.itemContent}>
      <Text style={styles.name}>{item.name}</Text>
      <Text style={styles.brand}>{item.brand}</Text>
      <Text style={styles.itemQuantity}>{item.itemQuantity}</Text>
      <View style={styles.itemButtonContent}>
        <Text style={styles.toCartText}>Quantity: {item.cartQuantity}</Text>
        <Text style={styles.cartPriceText}>
          $ {item.cartQuantity * item.price}
        </Text>
      </View>
    </View>
  </View>
);
const CartScreen = (props) => {
  const renderItem = ({item}) => {
    return <Item item={item} />;
  };
  const renderFooter = () => {
    return (
      <Text style={styles.totalText}>
        Total: $
        {props.route.params.cart.reduce((accumulator, element) => {
          return accumulator + element.cartQuantity * Number(element.price);
        }, 0)}
      </Text>
    );
  };
  console.log(props);
  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={props.route.params.cart}
        renderItem={renderItem}
        ListFooterComponent={renderFooter}
        keyExtractor={(item) => item.id}
      />
      <TouchableOpacity
        style={styles.goToCartButton}
        onPress={() => alert('Booking sucessful')}>
        <Text style={styles.goToCartText}>Complete your booking</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
    marginHorizontal: 8,
  },
  itemCointainer: {
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: 'red',
    alignItems: 'center',
    marginVertical: 4,
    borderColor: 'grey',
    borderWidth: 0.5,
  },
  itemContent: {
    flexDirection: 'column',
    paddingHorizontal: 14,
    flex: 1,
    justifyContent: 'space-around',
  },
  itemButtonContent: {flexDirection: 'row', justifyContent: 'space-between'},
  addButton: {backgroundColor: 'green', borderRadius: 16},
  addButtonText: {
    color: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  toCartContainer: {
    borderColor: 'green',
    borderRadius: 20,
    borderWidth: 0.5,
    flexDirection: 'row',
  },
  toCartText: {paddingVertical: 8, paddingHorizontal: 14, fontSize: 16},
  toCartVerticalLine: {height: '100%', width: 0.7, backgroundColor: 'green'},
  goToCartButton: {
    backgroundColor: 'green',
    marginVertical: 15,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  goToCartText: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    color: 'white',
    fontSize: 24,
  },
  image: {height: 120, width: 120, margin: 8},
  verticalLine: {height: 110, width: 0.5, backgroundColor: 'grey'},
  name: {
    fontSize: 18,
  },
  brand: {
    fontSize: 14,
    fontWeight: '600',
  },
  itemQuantity: {
    fontSize: 12,
  },
  price: {
    fontSize: 12,
  },
  cartPriceText: {fontSize: 24, fontWeight: '200'},
  totalText: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    fontSize: 24,
    fontWeight: '400',
  },
});
