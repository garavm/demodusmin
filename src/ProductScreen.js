import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    name: 'Atta - Whole wheate 1kg',
    brand: 'Aashirvaad',
    itemQuantity: '1 PC',
    cartQuantity: 0,
    uri: require('./assets/1.png'),
    price: '57',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    name: 'TYuye 1kg',
    brand: 'Aashirvaad',
    itemQuantity: '1 PC',
    cartQuantity: 0,
    uri: require('./assets/2.png'),
    price: '57',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    name: 'YUo 1kg',
    brand: 'Aashirvaad',
    itemQuantity: '1 PC',
    cartQuantity: 0,
    uri: require('./assets/3.png'),
    price: '57',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d74',
    name: 'Tesr 1kg',
    brand: 'Aashirvaad',
    itemQuantity: '1 PC',
    cartQuantity: 0,
    uri: require('./assets/2.png'),
    price: '57',
  },
];

const Item = ({item, increaseQuantity, decreaseQuantity}) => (
  <View style={styles.itemCointainer}>
    <Image source={item.uri} style={styles.image} />
    <View style={styles.verticalLine} />
    <View style={styles.itemContent}>
      <Text style={styles.name}>{item.name}</Text>
      <Text style={styles.brand}>{item.brand}</Text>
      <Text style={styles.itemQuantity}>{item.itemQuantity}</Text>
      <View style={styles.itemButtonContent}>
        <Text style={styles.price}>Price: ${item.price}</Text>
        {item.cartQuantity === 0 ? (
          <TouchableOpacity style={styles.addButton} onPress={increaseQuantity}>
            <Text style={styles.addButtonText}>ADD</Text>
          </TouchableOpacity>
        ) : (
          <View style={styles.toCartContainer}>
            <TouchableOpacity onPress={decreaseQuantity}>
              <Text style={styles.toCartText}>-</Text>
            </TouchableOpacity>
            <View style={styles.toCartVerticalLine} />
            <Text style={styles.toCartText}>{item.cartQuantity}</Text>
            <View style={styles.toCartVerticalLine} />
            <TouchableOpacity onPress={increaseQuantity}>
              <Text style={styles.toCartText}>+</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    </View>
  </View>
);
const ProductScreen = (props) => {
  const [data, setData] = useState(DATA);
  const [cart, setCart] = useState([]);
  const decreaseQuantity = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        return {...i, cartQuantity: i.cartQuantity - 1};
      } else {
        return i;
      }
    });
    setData(newData);
    setCart(newData.filter((i) => i.cartQuantity !== 0));
  };
  const increaseQuantity = (id) => {
    const newData = data.map((i) => {
      if (i.id === id) {
        return {...i, cartQuantity: i.cartQuantity + 1};
      } else {
        return i;
      }
    });
    setData(newData);
    setCart(newData.filter((i) => i.cartQuantity !== 0));
  };
  const onSubmit = () => {
    props.navigation.navigate('Cart', {cart});
  };

  const renderItem = ({item}) => {
    return (
      <Item
        item={item}
        decreaseQuantity={() => decreaseQuantity(item.id)}
        increaseQuantity={() => increaseQuantity(item.id)}
      />
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      {cart.length > 0 ? (
        <TouchableOpacity style={styles.goToCartButton} onPress={onSubmit}>
          <Text style={styles.goToCartText}>Go to Cart</Text>
        </TouchableOpacity>
      ) : null}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 8,
    marginHorizontal: 8,
  },
  itemCointainer: {
    flex: 1,
    flexDirection: 'row',
    // backgroundColor: 'red',
    alignItems: 'center',
    marginVertical: 4,
    borderColor: 'grey',
    borderWidth: 0.5,
  },
  itemContent: {
    flexDirection: 'column',
    paddingHorizontal: 14,
    flex: 1,
    justifyContent: 'space-around',
  },
  itemButtonContent: {flexDirection: 'row', justifyContent: 'space-between'},
  addButton: {backgroundColor: 'green', borderRadius: 16},
  addButtonText: {
    color: 'white',
    paddingVertical: 8,
    paddingHorizontal: 16,
  },
  toCartContainer: {
    borderColor: 'green',
    borderRadius: 20,
    borderWidth: 0.5,
    flexDirection: 'row',
  },
  toCartText: {paddingVertical: 8, paddingHorizontal: 14, fontSize: 16},
  toCartVerticalLine: {height: '100%', width: 0.7, backgroundColor: 'green'},
  goToCartButton: {
    backgroundColor: 'green',
    marginVertical: 15,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  goToCartText: {
    paddingVertical: 8,
    paddingHorizontal: 16,
    color: 'white',
    fontSize: 24,
  },
  image: {height: 120, width: 120, margin: 8},
  verticalLine: {height: 110, width: 0.5, backgroundColor: 'grey'},
  name: {
    fontSize: 18,
  },
  brand: {
    fontSize: 14,
    fontWeight: '600',
  },
  itemQuantity: {
    fontSize: 12,
  },
  price: {
    fontSize: 12,
  },
});

export default ProductScreen;
